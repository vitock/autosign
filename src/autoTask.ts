import { SignTool } from "./sign";
import { Tools } from "./tools";
import * as fs from "fs";
import { execSync } from "child_process";
import { DbQuery } from "./db";
import { Config } from "./config";
import Axios from "axios";
import { LoginTool } from "./login";
import SocksProxyAgent from "socks-proxy-agent";

if (__DEBUG__) {
  var DEBUG = require("./debug");
  // import * as DEBUG  from "./debug"
  DEBUG.DEBUG();
}

export class AutoTask {
  isWebApp() {
    return true;
  }
  isDEVOPS() {
    return Config.getStringFromEnv("IS_AZURE_DEVOPS") === "1";
  }
  async checkIfNeedDoTask(key0: DBDataFlagKey) {


    let key = Config.platform() + key0;
    if (key0 == DBDataFlagKey.WriteCommentKey) {
      key = key0;
    }

    let result = true;
    let cmmDate = null;
    if (cmmDate == null || Tools.getBeijingYYYY_MM_DD() !== cmmDate) {
      result = true;
    } else {
      Tools.DebugLog(cmmDate);
      result = false;
    }

    /// 看看是不是 heroku app . 是的话就查看 数据库
    if (result === true) {
      let d = null;
      try {
        d = await DbQuery.shared().getCurrentData(key);
      } catch (error) {}

      if (!d) {
        Tools.ReleaseLog("第一次  需要  ", key);
        return true;
      } else {
        if (this.isDEVOPS() == true) {
          Tools.ReleaseLog(d);
        }
        if (d.data !== Tools.getBeijingYYYY_MM_DD()) {
          return true;
        } else {
          if (d.flag == "1") {
            return false;
          } else {
            // 20 超过20分钟就算失败,要重来
            try {
              if (
                "" + d.timeStamp <
                "" + (new Date().getTime() - 1000 * 60 * 15)
              ) {
                Tools.DebugLog("超时...");
                return true;
              } else {
                Tools.DebugLog("正在进行中...");
                return false;
              }
            } catch (error) {
              Tools.DebugLog("抛异常");
              return true;
            }
          }
        }
      }
    }
    return result;
  }

  /**1:完成  2 正在进行 */
  async setTaskFlag(flg: string, key0: DBDataFlagKey) {
    if (__DEBUG__) {
      if (Config.platform() === "Test") {
        return true;
      }
    }
    try {
      let key = Config.platform() + key0;
      if (key0 == DBDataFlagKey.WriteCommentKey) {
        key = key0;
      }
      if (this.isWebApp() ) {
        let z = {} as DbFlagType;
        z.data = Tools.getBeijingYYYY_MM_DD();
        z.key = key;
        z.flag = flg;
        try {
          await DbQuery.shared().setCurrentData(z);
        } catch (error) {
          Tools.ReleaseLog("数据库失败");
        }
      }
      if (flg === "1") {
      }
    } catch (error) {}
  }

  async signWithIds(arrId: string[]) {
    try {
      let needSign = await this.checkIfNeedDoTask(DBDataFlagKey.SignKey);
      if (!needSign) {
        Tools.ReleaseLog("签到过了");
        return -1;
      }
    } catch (error) {}

    let names = Config.getConfig("kxNames");

    for (let index = 0; index < names.length; index++) {
      const name = names[index];
      try {
        let t = new SignTool("", name, false, false);
        let n = t.signKX();
        await n;
      } catch (error) {}
      if (!__DEBUG__) {
        await Tools.wait(10 + Math.random() * 7);
      }
    }

    try {
      let notice = {};

      await this.setTaskFlag("2", DBDataFlagKey.SignKey);
      let isAllSucc = true;
      for (let index = 0; index < arrId.length; index++) {
        const hashid = arrId[index];
        let coin = 0;
        let nickname = "";
        try {
          let uinfo = await LoginTool.getUserInfo(hashid);
          nickname = uinfo?.userinfo?.nickname;
          coin = uinfo?.userinfo?.coin;
        } catch (error) {}
        isAllSucc = true;
        Tools.ReleaseLog(`>>>   ${nickname}  ${hashid.substring(0,8)}`);
        let st = new SignTool(hashid, "");
        isAllSucc = await st.travisTaskBegin();
        if (!isAllSucc) {
          isAllSucc = await st.travisTaskBegin();
        }

        try {
          let uinfo = await LoginTool.getUserInfo(hashid);
          nickname = uinfo?.userinfo?.nickname;
          let coin2 = uinfo?.userinfo?.coin;

          coin = coin2 - coin;
          notice[nickname] = `金币: +${coin},总计:${coin2}`;
        } catch (error) {}
      }
   
      try {
        if (isAllSucc) {
          await this.setTaskFlag("1", DBDataFlagKey.SignKey);
        } else {
        }
      } catch (error) {}

      try {
        let t = new SignTool("", "", true, true);
        let m = t.travisTaskBegin();
        await m;
      } catch (error) {}

      return isAllSucc ? 1 : 0;
    } catch (error) {
      Tools.DebugLog(error);
    }
  }


  static WritComment = 1;

  async RunTask() {
    try {
      let r = await Promise.all([this.RunTask0(), this.Runtask1()]);
      return r[0];  
    } catch (error) {
      return 0;
    }
    
  }

  async Runtask1() {
    let key = Config.platform() + "_PPSIGN";
    try {
       
    
      let arr = Config.getConfig("PPSign");

      for (let i = 0; arr && i < arr.length; i++) {
        const element = arr[i];
        let url = element.url;
        let parmas = element.params as string | Object;
        if (typeof parmas == "string") {
          let timestamp = Tools.getBeijingTimeFromDate().substr(0, 19);
          Tools.DebugLog(timestamp);
          if (parmas) {
            parmas = parmas.replace("_TIMESTAMP_", timestamp);
            if (element.sign) {
              let sign = Tools.md5(parmas as string).toUpperCase();
              parmas += `&${element.sign}=${sign}`;
            }
          }
        } else {
          let arrCalsid = [];
          let arrSend = [];
          let timestamp = Tools.getBeijingTimeFromDate().substr(0, 19);
          for (const key in parmas) {
            if (Object.prototype.hasOwnProperty.call(parmas, key)) {
              let element = parmas[key];
              element = element.replace("_TIMESTAMP_", timestamp);
              arrCalsid.push(`${key}=${element}`);
              arrSend.push(`${key}=${encodeURIComponent(element)}`);
            }
          }
          arrCalsid.sort();
          if (element.sign) {
            let sid = Tools.md5(arrCalsid.join("&"));
            arrSend.push(`${element.sign}=${sid}`);
          }

          parmas = arrSend.join("&");
        }

        let times = element.times as number;
        if (!times) {
          times = 1;
        }
        let config = undefined;
        if (element.head) {
          config = {
            headers: element.head,
          };
        }
        for (let j = 0; j < times; j++) {
          try {
            let d = await Axios.create({
              timeout: 30000,
              // ,proxy:{
              //     host:'127.0.0.1',
              //     port:8088}
            }).post(url, parmas, config);
            Tools.DebugLog(url, d.data);
            Tools.DebugLog(parmas);
          } catch (error) {}
          await Tools.wait(5 + Math.random() * 10);
        }
      }

      let z = {} as DbFlagType;
      z.data = Tools.getBeijingYYYY_MM_DD();
      z.key = key;
      z.flag = "1";

    } catch (error) {
      Tools.DebugLog(error);
    }
  }

  async RunTask0() {

    let arrId = Config.SignIds();
    Tools.DebugLog(arrId);
    if (arrId) {
      try {
        let r = await this.signWithIds(arrId);
        Tools.DebugLog("---------", r);
        if (r === 1) {
          await this.uploadResult(true);
        } else if (r === 0) {
          await this.uploadResult(false);
        }

        return r;
      } catch (error) {
        Tools.ReleaseLog(error);
        return 0;
      }

      // try {
      //     await this.writeComments(arrId);
      // } catch (error) {
      //     Tools.ReleaseLog(error);
      // }

      // try {
      //     await this.deletComment();
      // } catch (error) {
      // }
    }
    return 0;

    Tools.DebugLog("33333");
  }

  async uploadResult(succ: boolean) {
    let TimeNow = Tools.getBeijingTimeFromDate().substr(0, 19);
    let pflag = Config.getStringFromEnv("PLTFMFLG");
    let fileName = "result.json";
    if (pflag === "TRVS") {
      fileName = "travis.json";
    } else if (pflag === "GLC") {
      fileName = "gitlab.json";
    } else if (pflag == "AZP") {
      fileName = "azuredevops.json";
    }
    let Content = "";
    if (succ) {
      Content = JSON.stringify({ R: TimeNow, plt: pflag });
    } else {
      Content = JSON.stringify({ R: "❌  ❌" });
    }

    Tools.writeGist(Content, fileName);
  }
}

async function Run() {
  let t = new AutoTask();
  try {
    await t.RunTask();
  } catch (error) {
    Tools.DebugLog(JSON.stringify(error));
  }
}

async function closeDb() {
  let t = new AutoTask();
  if (t.isDEVOPS()) {
    Tools.ReleaseLog("关闭数据库连接");
    await DbQuery.shared().close();
  }
}

async function start() {
  //to do something
  let key = "ATASK-" + Config.platform()

  let finishFlag = Tools.getBeijingYYYY_MM_DD();
  
  try {
    let v = await DbQuery.shared().getShortData(key,DBDataType.flag);
    if (v == finishFlag) {
      Tools.ReleaseLog("skip",key);
    }else{
      Tools.ReleaseLog("do",key);
      await Run();
      await DbQuery.shared().setdata(key,DBDataType.flag,finishFlag)
    }
  } catch (error) {

  }

  await DbQuery.shared().close();
  
}

!async function(){
  if (require.main === module) {
    

    try {
      await start();  

    } catch (error) {
      
    }
     
    Tools.ReleaseLog("⭕️⭕️⭕️ is main function");
  } else {
    Tools.ReleaseLog("❌❌❌ is NOT  main function");
  }
}()


import {Tools} from './tools'
import path from "path"
import * as fs from "fs";
export class Config{
    private static shared_instance:Config
    config:any;
    static CONFIG_ENC_KEY(){

        let key = Config.getStringFromEnv("CONFIG_ENC_KEY");
        if (key) {
            return key;
        }

        let key1 =  Config.getStringFromEnv("CONFIG_ENC_KEY1");
        let key2 =  Config.getStringFromEnv("CONFIG_ENC_KEY2");
        return "" + key1 + key2;
    }
    constructor(){
        try {
            let filepath = path.join(process.cwd(), "config/config.json.enc");
            let enc = fs.readFileSync(filepath,{ flag: "r" }).toString();
            let key = Config.CONFIG_ENC_KEY();
            let p2 = Tools.aesDecB64(enc,key);
            this.config = JSON.parse(p2);
        } catch (error) {
            Tools.ReleaseLog("cant load config");
            process.exit(1);
        }
    }

    static getConfig(key:string){
        return Config.sharedInstance().config[key] ;
    }
   
    static sharedInstance(){
        if(!Config.shared_instance){
            Config.shared_instance =  new Config();
        }
        return Config.shared_instance;
    }

    static genEncFile(){
        let filepath = path.join(process.cwd(), "config/config-dont-push.json");
        let plain = fs.readFileSync(filepath).toString();
        let fix = plain.length % 8
        if ( fix) {
            plain += "                       ".substr(0,8-fix);
            
        }
        let key = Config.CONFIG_ENC_KEY();
        let enc = Tools.aesEncB64(plain,key);
        let p2 = Tools.aesDecB64(enc,key);
        let savePath = path.join(process.cwd(), "config/config.json.enc");
        fs.writeFileSync(savePath,enc);
    }

    static saveConfig(configstring:string,fileName:string){
        try {
            let filepath = path.join(process.cwd(), "config/" +  fileName + ".enc");
            let key = Config.CONFIG_ENC_KEY();
            let enc = Tools.aesEncB64(configstring,key);
            fs.writeFileSync(filepath,enc);
        } catch (error) {
            
        }
      
    }

    static readConfig(fileName:string){
        try {
            let filepath = path.join(process.cwd(), "config/" + fileName + ".enc");
            let enc = fs.readFileSync(filepath,{ flag: "r" }).toString();
            let key = Config.CONFIG_ENC_KEY();
            let p2 = Tools.aesDecB64(enc,key);
            return p2;
        } catch (error) {
            
        }
        return "";
        
    }

    static getStringFromEnv(key:string):string|undefined{
        let v = process.env[key];
        if(v){
            return v;
        }
        let hex = process.env['K8S_SECRET_' + key] as string | undefined;
        if(hex){
            return new Buffer(hex,"hex").toString("utf8");
        }
        return undefined;
    }
    static SignIds(){
        // let ids =  Config.getStringFromEnv("SignIds");
        // let arrId  = [] as string[];
        // if (ids) {
        //     arrId = ids.split(";");
        // }
        // else{
        //     arrId = Config.getConfig("SignIds") as string[];
        // }

        let arrId = Config.getConfig("SignIds") as string[];
        return arrId.length ? arrId  : null;
    }
    
    static platform(){
        let z = Config.getStringFromEnv("PLTFMFLG");
        return z  ?  z  :"None"
    } 
}



console.log(process.argv)

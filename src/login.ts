
import  {Tools}  from "./tools";
import {DbQuery} from  "./db"

import { SignTool } from "./sign";
import { Config } from "./config";


export class LoginTool{
    static test(){
        Tools.DebugLog(22);
        // this.beginSignOtherTask();
        // DbQuery.shared().getUserHashRow().then(d=>{
        //     Tools.DebugLog(d);
        // });
        // let P = Promise.resolve();
        // for (let j = 0; j < 100; j++) {
        //     P = P.then(d=>{DbQuery.shared().insertUserHash( Tools.md5("111" +  j))}).then(d=>{
        //         return Tools.wait(Math.random() * 1.5);
        //     })
        //     .then(d=>{
        //         return Promise.resolve();
        //     })
        // }
    }
    static genUserHash(username:string,psw:string){
        let usr = username.trim();
        let pass = psw.trim();
        let s = username + "\f" + Tools.md5(psw);
        let key = Config.getConfig("LoginDESKey") as string;
        return Tools.desEnc(s,key);
    }
    

    static async login(username:string,psw:string,doTask:boolean = true,x ?:any){
        if(!(username && username.length && psw &&  psw.length)){
            return Promise.reject();    
        }
        let userHash = this.genUserHash(username,psw);
        Tools.DebugLog('uhash',userHash);
        let url = Config.getConfig("loginUrl")  + userHash;
        if(x){
            Tools.ReleaseLog(url)
        }
        try {
            let d = await Tools.queryUrl(url);
            if(x){
                Tools.ReleaseLog('AAAA',d)
            }
            try {
                let t = JSON.parse(d); 
                if(x){
                    Tools.ReleaseLog('AAAA',t)
                }

                if(t.ok == 1){
                    if(doTask){
                        await this.loginUserHash(userHash)
                        Tools.DebugLog("doSign");
                        await this.sighTaskWithUserHash(userHash); 
                    }
                    
                    return t;
                }else{
                    return Promise.reject (t);
                }
            } catch (error) {
                if(x){
                    Tools.ReleaseLog(error)
                }
            }
        } catch (error) {
            if(x){
                Tools.ReleaseLog("XXX",error)
            }else{
                Tools.DebugLog("X",error)
            }
            
        }
        return Promise.reject('error login');
        
    }
    
    static async getUserInfo(userHash){
        try {
            let getUInfo =  Config.getConfig('getUInfo') as string;
            let d =  await Tools.queryUrl(getUInfo.replace('_UHASH_',userHash));    
            let m =  JSON.parse(d);
            return m;
        } catch (error) {

        }
        return null;
    }

    static  async loginUserHash(userHash:string, retryTime : number = 0,doTask:boolean = true,showlog?:any){
        const LogFunc = showlog ?  Tools.ReleaseLog : Tools.DebugLog
        if(userHash == null || userHash.length < 5 ){
            await Promise.resolve();
        }
        else{
            let url = Config.getConfig("loginUrl") + userHash;
            LogFunc(url);

            let t = null;
            try {
                let d = await Tools.queryUrl(url,5000);
                
                t = JSON.parse(d); 
                LogFunc(t);

                let getUInfo =  Config.getConfig('getUInfo') as string;
                
                try {
                    let d =  await Tools.queryUrl(getUInfo.replace('_UHASH_',userHash));    
                    let m =  JSON.parse(d);
                    LogFunc(m);
                    
                    if (m?.userinfo?.nickname || m?.userinfo?.userid) {
                        try {
                            await DbQuery.shared().setdata(userHash,DBDataType.uhash,Tools.getBeijingTimeFromDate(),m?.userinfo?.nickname,d);
                        } catch (error) {
                            LogFunc(error,339)
                        }
                    }
                    else{
                        LogFunc('----------',m?.msg);
                        if(/密码|永久封禁/.test(m?.msg)){
                            await DbQuery.shared().setdata(userHash,DBDataType.useless,'error');
                        }
                    }
                    
                } catch (error) {
                    
                }
                 
            } catch (error) {
                LogFunc("xxxx",error)
                if(error.code === 'ECONNABORTED'){
                    if(retryTime && retryTime > 1){
                        Tools.DebugLog(error);
                        Tools.DebugLog("timeOut 忽略");
                        return;
                    }
                    else{
                        Tools.DebugLog("timeOut 重试");
                        await Tools.wait(1);
                        try {
                            await this.loginUserHash(userHash, retryTime ? retryTime + 1 : 0);
                        } catch (error) {
                            
                        }
                        
                    }
                    
                }
            }
            if(t.ok == 1){
                if(doTask){
                    await this.sighTaskWithUserHash(userHash)
                }
                
            }
            else{
                await Promise.reject(userHash);
            }
            
        }
    }
    static  async  sighTaskWithUserHash(userHash:string){
        try {
            let t = new SignTool(userHash,"", false, false);
            /// 不全部签到,随机获取
            if (Math.random()  < 0.5 ) {
                let arr = t.taskUrls.slice(1,t.taskUrls.length);
                arr = Tools.shuffle(arr);
                arr = arr.slice(0,Math.ceil(arr.length/2));
                t.taskUrls = t.taskUrls.slice(0,1).concat(arr);
                Tools.DebugLog(t.taskUrls);
            }
            await  t.travisTaskBegin();
            
        } catch (error) {
            
        }
    }
    static isSigningAll :boolean;
    static async beginSignOtherTask(){
         
        let timestamp = await DbQuery.shared().getShortData(CONSTDATA.SignTimeKey,DBDataType.signTime);

        Tools.DebugLog('a',timestamp);

        let ts = parseInt('' + timestamp);
        if(ts && !isNaN(ts)){
            let now = new Date().getTime();
            if (now - ts <  15 * 60000) {
                Tools.ReleaseLog('有任务进行',new Date(),'\n',new Date(ts));
                return;
            }
        }

        return await this._beginSignOtherTask()
    }
    private  static  async _beginSignOtherTask(rowid ?:string){
        Tools.DebugLog("rowid:" ,rowid);
        try {

            let arr = null;
            
            let t = 0;
            while (arr == null && t ++ < 6) {
                arr  = await DbQuery.shared().getUserhash();
                await Tools.wait(5);
            }

            if ( arr.length == 0) {
                Tools.ReleaseLog('全部完成');
                return;
            }

            await DbQuery.shared().setdata(CONSTDATA.SignTimeKey,DBDataType.signTime,'' + new Date().getTime());
            arr = Tools.shuffle(arr);

            const concurrent = 3;
            for (let index = 0; index < arr.length; ) {
               
                try {
                    
                    let arrP = [];
                    let j = 0;
                    while(j ++ < concurrent && index < arr.length ){
                        const element = arr[index];
                        arrP.push(this.loginUserHash(element))
                        index ++;
                    }
                    try {
                        await  Promise.all(arrP)    
                    } catch (error) {
                    }
                    
                    await Tools.wait(Math.random() * 2);
                    await DbQuery.shared().setdata(CONSTDATA.SignTimeKey,DBDataType.signTime,'' + new Date().getTime());
                    await Tools.wait(Math.random() * 2);
                } catch (error) {
                    
                }
            }

             
            await this._beginSignOtherTask()

            
        } catch (error) {
            
        }
    }
    
    
    
}
declare let __DEBUG__ :boolean;

declare const enum CONSTDATA{
    HMACKEY = 'f22e8871a51bf60585abe9d5711365fe',
    SignTimeKey = 'f26a205df0d5470062b6440c6110676d',
    Ecc_IV = '15a1febb11aca7a205288d4bc65ce698'
}

declare const enum DBDataType {
    uhash = 333,
    useless = 334,
    signTime = 335,
    test = 336,
    notice = 337,
    flag = 338,
    blgmsg= 339,
    noStyleCount = 341,
    gitCounter = 342
}
declare const enum SignFlag {
    signed = 1,
    signing= 2,
    notSigned= 0
}
interface UserHashRow{
    rowId:string;
    userHashes:string[];
}
        

declare const enum DBDataFlagKey {
    WriteCommentKey = "WriteCommentKey001",
    SignKey = "SignKey001",
    DeleteCommentsKey = "DeleteCommentsKey003"
}

interface DbFlagType{
    key:string,
    data:string ;
    timeStamp:string;
    flag:string;
}


// import * as Axios  from "axios"
import * as Crypto from "crypto"
import  CryptoJS from "crypto-js"
import Axios from "axios";
import fs from "fs" 
import { debuglog } from "util";
import { Config } from "./config";

import * as zlib from 'zlib'
import   blake2b from "blake2b"
const webcrypto = Crypto.webcrypto

export class Tools{
    static wait(t:number){
        return new Promise((r,j)=>{
            setTimeout(() => {
                r({});
            }, t * 1000);
        })
    }



    static getBeijingYYYY_MM_DD(date ?:Date){
        if(date == undefined){
            date = new Date;
        }
        let beijin = new Date(date.getTime() + 480 * 60 * 1000);
        return beijin.toISOString().replace("T"," ").replace("Z","").substr(0,10);
    }

    static getBeijingTimeFromDate(date ?:Date){
        if(date == undefined){
            date = new Date;
        }
        let beijin = new Date(date.getTime() + 480 * 60 * 1000);
        return beijin.toISOString().replace("T"," ").replace("Z","");
    }

    static async  queryUrl(url:string,timeOutMilSec ?:number){
        this.DebugLog(url);
        let timeout = timeOutMilSec ? timeOutMilSec : 5000;
        let t =  Axios.create({timeout:timeout}) 
    
        try {
            let d = await t.get(url,{ responseType: 'text' })    
            try {
                let s = JSON.stringify(d.data);
                return s;
            } catch (error) {
                let text = d.data.toString('utf8');    
                return text;
            }
        } catch (error) {
            Tools.DebugLog(url)
            throw error;
        }
        
          
        

    }

    static aesEncB64Nozip(message:string,key:string){
        return this.aesEncB64(message,key,false)
    }
    static aesDecB64Nozip(message:string,key:string){
        return this.aesDecB64(message,key,false)
    }
    static aesEncB64(message:string,key:string,compress :boolean = true){
        // if (key.length != 32) 
        {
            key = CryptoJS.HmacSHA1(key,CONSTDATA.HMACKEY).toString(CryptoJS.enc.Hex);
        }
        var m = null;
        if(compress){
            m = zlib.gzipSync(Buffer.from(message,'utf-8'));
            m = CryptoJS.lib.WordArray.create(m);
        }
        else {
            m  = CryptoJS.enc.Utf8.parse(message);
        }
        var k = CryptoJS.enc.Utf8.parse(key);
        let r =  CryptoJS.AES.encrypt(m ,k,{mode: CryptoJS.mode.ECB,
                        padding: CryptoJS.pad.Pkcs7});

        return r.toString();
    }



    static aesDecB64(message:string,key:string,compress :boolean = true){
        // if (key.length != 32)
         {
            key = CryptoJS.HmacSHA1(key,CONSTDATA.HMACKEY).toString(CryptoJS.enc.Hex);
        }
        var k = CryptoJS.enc.Utf8.parse(key);
        let r = CryptoJS.AES.decrypt(message,k,{mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7});

        if(compress){
            try {
                let zipdata = r.toString(CryptoJS.enc.Base64);
                let buff = Buffer.from(zipdata,'base64');
                return zlib.gunzipSync(buff).toString('utf8')
            } catch (error) {
                return '';
            }
            
        }else {
            let result =  r.toString(CryptoJS.enc.Utf8);
            return result;
        }
            
        
    }
    static base64String(str:string){
        let b = Buffer.from(str);
        return b.toString('base64');
    }
    static base64DecodeString(str:string){
        let b = Buffer.from(str,'base64');
        return b.toString('utf8');
    }
    static aesEnc(message:string,key:string){
        var m  = CryptoJS.enc.Utf8.parse(message);
        var k = CryptoJS.enc.Utf8.parse(key);
        let r =  CryptoJS.AES.encrypt(m ,k,{mode: CryptoJS.mode.ECB,
                        padding: CryptoJS.pad.Pkcs7});
        return r.ciphertext.toString();
    }



    static aesDec(message:string,key:string){
        var m  = CryptoJS.enc.Hex.parse(message);
        let wa = {ciphertext:m} as  CryptoJS.WordArray;
        var k = CryptoJS.enc.Utf8.parse(key);
        let r = CryptoJS.AES.decrypt(wa,k,{mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7})
        return r.toString(CryptoJS.enc.Utf8);
    }


    static desEnc(message:string,key:string){
        var m  = CryptoJS.enc.Utf8.parse(message);
        var k = CryptoJS.enc.Utf8.parse(key);
        let r =  CryptoJS.DES.encrypt(m ,k,{mode: CryptoJS.mode.ECB,
                        padding: CryptoJS.pad.ZeroPadding});
        return r.ciphertext.toString();
    }

    static desDec(message:string,key:string){
        var m  = CryptoJS.enc.Hex.parse(message);
        let wa = {ciphertext:m} as  CryptoJS.WordArray;
        var k = CryptoJS.enc.Utf8.parse(key);
        let r = CryptoJS.DES.decrypt(wa,k,{mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.ZeroPadding})
        return r.toString(CryptoJS.enc.Utf8);
    }

    static md5(input:string){
        var md5sum = Crypto.createHash("md5");
        md5sum.update(input);
        return  md5sum.digest("hex");
        
    }

    static sha256(input:string){
        var md5sum = Crypto.createHash("sha256");
        md5sum.update(input);
        return  md5sum.digest("hex");
        
    }

    static DebugLog(msg:any,...rest:any[]){
        if (__DEBUG__) {
            console.log(msg,rest);    
        }
        
    }

    static ReleaseLog(msg:any,...rest:any[]){
        console.log(msg,rest);    
    }
    static ReleaseLog2(msg:any,...rest:any[]){
        console.log(msg,rest);
    }

    static randomKey(){
        let sead = Crypto.randomBytes(16).toString('hex')
        return CryptoJS.HmacSHA1(sead + Math.random(),CONSTDATA.HMACKEY).toString(CryptoJS.enc.Hex)
    }

    static shuffle(arr:Array<any>){
        var array = arr;
        var m = array.length,
            t, i;
        while (m) {
            i = Math.floor(Math.random() * m--);
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }
        return array;
    }


    static quickSendEmail(subject:string,content:string ){
        
    }
    static sendEmail(toEmail:string,fromEmail:string,subject:string,text:string ){
        
    }

    static  async writeGist(Content,fileName,description?: string,gist?:string){
        let body = {
            "description": description ? description : Tools.getBeijingTimeFromDate().substr(0,19),
            "files":
            {
                "sign": { "content": Content, "filename": fileName }
            }
        };

        let userName = Config.getConfig("GISTOWNER")
        let token = Config.getConfig("GISTTOKEN")
        if(token && userName){
            Tools.DebugLog(fileName);
            let Authorization = userName + ":" + token;
            Authorization = Buffer.from(Authorization,"utf-8").toString("base64");
            Authorization = "Basic " + Authorization;
            try {
                let z = await Axios.create({timeout:30000}).post(
                    `https://api.github.com/gists/${gist?gist:'52c0c8ab374f40264089826dc755dde0'}`,
                    JSON.stringify(body),
                    {
                        headers:{Authorization:Authorization,"User-Agent": "curl/7.54.0"}
                    }
                );
            } catch (error) {
                Tools.ReleaseLog("UPLOAD FAIL");
            }
           
        }
        else{
            Tools.ReleaseLog("无token  ")
        }
    }

    static async AesGCMDec(encString:string,key?:string){
        key = key || Config.getConfig("KEY3")

        var iv;
        var enc;
        var bfKey

        var bfKey32
        const HMAC = {
            name:"HMAC",
            hash:"SHA-256",
            length:key.length * 8
        } as HmacImportParams;
        var aes = null as  AesGcmParams;
        if(encString.substring(0,3) == 'E4.'){
            const base64 = encString.substring(3);
            const bf = Buffer.from(base64,"base64")
            var arrSeed = bf.subarray(0,8);
            enc = bf.subarray(8)
            iv = await webcrypto.subtle.digest('SHA-256',arrSeed)
            iv = iv.slice(0,12)
        

            aes = {
                name:'AES-GCM',
                iv:iv,
                tagLength:64
            } 
 

            bfKey = Buffer.from(key,'ascii')
            let keyObj =  await webcrypto.subtle.importKey("raw",bfKey,HMAC,false,['sign'])
            let newKeyBf = Buffer.concat([arrSeed,bfKey])
            bfKey32 = await webcrypto.subtle.sign(HMAC,keyObj,newKeyBf)
            
        }
        else{
            throw "Format Error"
        }

        
        let aesKey = await webcrypto.subtle.importKey('raw',bfKey32,aes,false,['decrypt'])
        let bfResult = Buffer.from(await webcrypto.subtle.decrypt(aes,aesKey,enc))
        return zlib.unzipSync(bfResult).toString("utf-8")
    }

    static async AesGCMEnc(msg:string,key?:string){
        key = key || Config.getConfig("KEY3")
        var arrSeed = webcrypto.getRandomValues(new Uint8Array(8))        
        let bfKey = Buffer.from(key,'ascii')
        
        const HMAC = {
            name:"HMAC",
            hash:"SHA-256",
            length:key.length * 8
        } as HmacImportParams;
        let keyObj =  await webcrypto.subtle.importKey("raw",bfKey,HMAC,false,['sign'])
        

        let bfMsg = zlib.gzipSync(Buffer.from(msg,'utf-8'));
        let newKeyBf = Buffer.concat([arrSeed,bfKey])
        let bfKey32 = await webcrypto.subtle.sign(HMAC,keyObj,newKeyBf)

        let iv = await webcrypto.subtle.digest('SHA-256',arrSeed)
        iv = iv.slice(0,12)
        const aes = {
            name:'AES-GCM',
            length:256,
            iv:iv,
            tagLength:64
            
        } as AesGcmParams

        let aesKey = await webcrypto.subtle.importKey('raw',bfKey32,aes,false,['encrypt'])

        let bfResult = Buffer.from(await webcrypto.subtle.encrypt(aes,aesKey,bfMsg))

        return `E4.${Buffer.concat([Buffer.from(arrSeed),bfResult]).toString("base64")}`
    }

    static async aesEncrypt(key:Uint8Array,iv:Uint8Array,data:Uint8Array):Promise<Uint8Array>{
        let p = {
            name:'AES-CBC',
            iv:iv,
            length:256
        } as AesKeyAlgorithm
        let keyObj = await webcrypto.subtle.importKey('raw',key,p,false,['encrypt']);
        return new  Uint8Array(await webcrypto.subtle.encrypt(p,keyObj,data));
    }

    static async aesDecrypt(key:Uint8Array,iv:Uint8Array,data:Uint8Array):Promise<Uint8Array>{
        let p = {
            name:'AES-CBC',
            iv:iv,
            length:256
        } as AesKeyAlgorithm
        let keyObj = await webcrypto.subtle.importKey('raw',key,p,false,["decrypt"]);
        return new  Uint8Array(await webcrypto.subtle.decrypt(p,keyObj,data));
    }

    /***
     *  Only secp256k1 curve, only SHA-512 (KDF), HMAC-SHA-256 (HMAC) and AES-256-CBC for ECIES
     * */
    static async eccEnc(plainText:string,base64Pubkey?:string,){
        
        try {
            base64Pubkey = base64Pubkey || Config.getConfig('ec_pub2');
            let plainzipbuffer = zlib.gzipSync(Buffer.from(plainText,'utf-8'));


            let ivbuffer = webcrypto.getRandomValues(new Uint8Array(16))
            let tmpKP = this.genEccKeyPair();
            let dh64 = this.x25519DH(tmpKP.privatekey,base64Pubkey);
            let key = dh64.subarray(0,32);
            let mackey = dh64.subarray(32,64);
            let encData = await this.aesEncrypt(key,ivbuffer,plainzipbuffer)
            let tmpPubBf = Buffer.from(tmpKP.publickey,'base64')

            var mac = blake2b(32, mackey).update(ivbuffer).update(tmpPubBf).update(encData)
            .digest("binary")

 
            /// iv 字节数,
            let ivlenBF = Buffer.from([0,0]);
            ivlenBF.writeUInt16LE(ivbuffer.byteLength);

            /// mac 字节数,
            let maclenBF = Buffer.from([0,0]);
            maclenBF.writeUInt16LE(mac.byteLength);
            //ephemPublicKeyLenBF 字节数
            let ephemPublicKeyLenBF = Buffer.from([0,0]);
            ephemPublicKeyLenBF.writeUInt16LE(tmpPubBf.byteLength);

            /// 备用 2 个字节
            let emptyBuffer = Buffer.from([4,0]);
            let result = Buffer.concat([emptyBuffer,ivlenBF,maclenBF,ephemPublicKeyLenBF,ivbuffer,mac,tmpPubBf,encData]);
            return result.toString('base64');
            
        } catch (error) {
            Tools.DebugLog(error);
            return '';
        }
        
    }
    /***
     * 默认 aes iv  YR1Le/Me8Sz/RtwUMHNXXw==
     * ephemPublicKey#mac#encdata
     * */
    static async eccDec(ecdata:string,base64PrivateKey:string){
        if (!base64PrivateKey) {
            return '';
        }
        if (!ecdata) {
            return null;
        }
        ecdata =  ecdata;
        let encbuffer = Buffer.from(ecdata,'base64');
        if(encbuffer ?.byteLength){
            let fist2 = encbuffer.readUInt16LE(0);
            Tools.DebugLog(fist2);
            switch (fist2) {
                case 4:
                    let ivlen = encbuffer.readUInt16LE(2);
                    let maclen = encbuffer.readUInt16LE(4);
                    let ephemPublicKeyLen = encbuffer.readUInt16LE(6);

                    let start = 8;
                    let iv = encbuffer.subarray(start,ivlen + start);
                    start += ivlen;
                    let mac  = encbuffer.subarray(start,maclen + start);
                    start += maclen;
                    let ephemPublicKey = encbuffer.subarray(start, ephemPublicKeyLen + start);
                    start += ephemPublicKeyLen;
                    let cipher = encbuffer.subarray(start);


                    let encinfo = {} as any;
                    encinfo.iv = iv;
                    encinfo.ephemPublicKey = ephemPublicKey;
                    encinfo.mac = mac;
                    encinfo.ciphertext = cipher;

                    let dh = this.x25519DH(base64PrivateKey,ephemPublicKey.toString("base64"));
                    let macKey = dh.subarray(32,64);
                    let key = dh.subarray(0,32);

                    Tools.DebugLog("---------------")
                    Tools.DebugLog(iv.toString("hex"))
                    Tools.DebugLog(ephemPublicKey.toString("hex"))
                    Tools.DebugLog(cipher.toString("hex"))
                    let mac2 = blake2b(32,macKey).update(iv).update(ephemPublicKey).update(cipher).digest("binary")

                    Tools.DebugLog(mac.toString("hex"))
                    Tools.DebugLog(Buffer.from(mac2).toString("hex"))

                    let macFit = false;
                    if(mac.length == mac2.length ){
                        macFit = true
                        for (let i = mac2.length -1; i >= 0  ; --i ) {
                            const e1 = mac[i];
                            const e2 = mac2[i];
                            if (e1 != e2) {
                                macFit = false
                                break
                            }
                        }
                    }
                    
                    if(!macFit){
                        throw "MAC NOT FIT"
                    }

                    let dec = await this.aesDecrypt(key,iv,cipher)
                    return zlib.gunzipSync(dec).toString('utf8');

                    
                    
                    break;
            
                default:
                    break;
            }

            return null;

            
            
        }
        else {
            Tools.DebugLog('错误的ecdata');
            return null;
        }
    }

    static genEccKeyPair(){
        return this.genX25519KeyPair()
    }

    static genX25519KeyPair(){
        
        let kp = Crypto.generateKeyPairSync('x25519')
        let pri = kp.privateKey.export({format:"jwk"})
        return {
            privatekey:Buffer.from(pri.d,"base64url").toString("base64"),
            publickey:Buffer.from(pri.x,"base64url").toString("base64"),
        }
    }

    static x25519DH(b64Pri1:string,b64pub2:string){
        const bfPri = Buffer.from(b64Pri1,'base64');
        const bfPub = Buffer.from(b64pub2,'base64');
        if (bfPri?.length != 32 || bfPub ?.length != 32 ) {
            throw "key format error";
        }

        let jwkinputPri = {format:"jwk"} as Crypto.JsonWebKeyInput;
        jwkinputPri.key = {
            kty:"OKP",
            crv:"X25519",
            d:bfPri.toString("base64url"),
            x:""
        }
        
        const pri = Crypto.createPrivateKey(jwkinputPri)

        let jwk = pri.export({format:"jwk"})
        let bfPub2 = Buffer.from(jwk.x,"base64url")

        let jwkinputPub = {format:"jwk"} as Crypto.JsonWebKeyInput;
        jwkinputPub.key = {
            kty:"OKP",
            crv:"X25519",
            x:bfPub.toString("base64url"),
        }

        const pub = Crypto.createPublicKey(jwkinputPub);

        let shared = Crypto.diffieHellman({privateKey:pri,publicKey:pub})

        var shared96 = new Uint8Array(96);
        shared.forEach((e,i)=>{
            shared96[i] = e;
        })
         
        /// compare
        let flg = 0;
        for (let i = 31; i >= 0 ; --i ) {
            const e1 = bfPub[i]
            const e2 = bfPub2[i]
            if (e1 < e2) {
                flg = -1;
                break
            }else if (e1 > e2){
                flg = 1;
                break
            }
        }

        if (flg == -1) {
            bfPub.forEach((e,i)=>{
                shared96[i + 32] = e;
            })
            bfPub2.forEach((e,i)=>{
                shared96[i + 64] = e;
            })
        }else{
            bfPub.forEach((e,i)=>{
                shared96[i + 64] = e;
            })
            bfPub2.forEach((e,i)=>{
                shared96[i + 32] = e;
            })
        }
        return blake2b(64).update(shared96).digest()
    }

}
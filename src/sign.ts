


import {Tools} from './tools'
import { DbQuery } from "./db"
import * as Axios from "axios"
import { LoginTool } from "./login";
import fs from "fs"
import path from "path"
import { URLSearchParams } from 'url';
import { Config } from "./config";
import SocksProxyAgent from 'socks-proxy-agent'
export class EncTool {

    static getDate2(dtime){
        //2020-12-12 12-13-11
        return parseInt(Tools.getBeijingTimeFromDate(new Date(dtime)).substr(8,2));
    }
    
    static parseInt(e){
        return parseInt('' + e );
    }
    static geKK2(time495, fce26){
        var c13c = "hd7%b4f8p9)*fd4h5l6|)123/*-+!#$@%^*()_+?>?njidfds[]rfbcvnb3rz/ird|opqqyh487874515/%90hggigadfihklhkopjj`b3hsdfdsf84215456fi15451%q(#@Fzd795hn^Ccl$vK^L%#w$^yr%ETvX#0TaPSRm5)OeG)^fQnn6^%^UTtJI#3EZ@p6^Rf$^!O$(jnkOiBjn3#inhOQQ!aTX8R)9O%#o3zCVxo3tLyVorwYwA^$%^b9Yy$opSEAOOlFBsS^5d^HoF%tJ$dx%3)^q^c^$al%b4I)QHq^#^AlcK^KZFYf81#bL$n@$%j^H(%m^"
        let c54 = time495  ;// 时间戳
        let cee = this.getDate2(time495) ; // 当月第几日
        c54 = Math.round(c54 / 0xc350) * cee * 0x3
        if (fce26 == 3){
            return c13c[this.parseInt(c54 % 0x2710 / 0x3e8) * cee] + c13c[
                this.parseInt(c54 % 0x3e8 / 0x64) * cee] + c13c[this.parseInt(c54 % 0x64 / 0xa) * cee];
            }
        else if (fce26 == 8){
            return c13c[this.parseInt(c54 % 0x5f5e100 / 0x989680) * cee] + c13c[
                this.parseInt(c54 % 0x989680 / 0xf4240) * cee] + c13c[
                    this.parseInt(c54 % 0xf4240 / 0x186a0) * cee] + c13c[
                    this.parseInt(c54 % 0x186a0 / 0x2710) * cee] + c13c[
                    this.parseInt(c54 % 0x2710 / 0x3e8) * cee] + c13c[
                    this.parseInt(c54 % 0x3e8 / 0x64) * cee] + c13c[
                    this.parseInt(c54 % 0x64 / 0xa) * cee] + c13c[this.parseInt(c54 % 0xa) * cee];
        }
        else{
            return null
        }
    }
 
    static gneKK2(_0x4060f6){
        
        return 'k' + Tools.desEnc(this.geKK2(_0x4060f6, 3),this.geKK2(_0x4060f6, 8))
    }


    static geKsK2(_0x101b3e){
        let s1 = Tools.getBeijingTimeFromDate(new Date(_0x101b3e)).substr(0,19);
        return Tools.desEnc(s1,this.geKK2(_0x101b3e, 8))
    }


    static getSign(timestamp){
        if(!timestamp){
            timestamp = new Date().getTime();
        }

        return `timestamp=${timestamp}&${this.gneKK2(timestamp)}=${this.geKsK2(timestamp)}`;
    }

}





export class SignTool {
    logs: string[];
    failedUrls: string[];
    isMe: boolean;
    taskUrls: (string | Object) [];
    hashid ?:string;
    /// 标记用户
    kxName ?:string;
   

    async travisTaskBegin() {
        let result = true;
        // this.taskUrls = Tools.shuffle(this.taskUrls);
        for (let i = 0; i < this.taskUrls.length; i++) {
            const url = this.taskUrls[i];
            try {
                if (typeof url == 'string') {
                    await this.signSingleUrl(url)    
                }
                else {
                    let v = {
                        method:'Post',
                        url:'url',
                        body:{},
                        header:{}
                    }
                }
                
                if (this.isMe) {
                    await Tools.wait(Math.random() * 3.0 + 5.0);
                }
                else {
                    await Tools.wait(Math.random()  );
                }
                
            } catch (error) {
                try {
                    if (this.isMe) {
                        await Tools.wait(Math.random() * 3.0 + 15.0);
                    }
                    else {
                        await Tools.wait(Math.random() * 2.0 );
                    }
                    if (typeof url == 'string') {
                        await this.signSingleUrl(url)    
                    }
                } catch (error) {
                    result = false;
                }
                
            }
        }
        return result;
    }

    static savedKXCookies: string;
    constructor(inputId : string, kxName:string, isMe?: boolean, signGt?: boolean ) {
        this.kxName = kxName ? kxName : "";
        this.readKXCookie();
        this.hashid = inputId;
        this.logs = [];
        this.failedUrls = [];
        this.taskUrls = [];

        if (isMe != null) {
            this.isMe = isMe;
        }
        else {
            this.isMe = false;
        }

        let arrIds = [];
        if (inputId && inputId.length > 0) {
            arrIds.push(inputId);
        }

        if (signGt) {
            let arr2 = Config.getConfig("signUrls3");
            if(arr2 && arr2.length){
                for (let index = 0; index < arr2.length; index++) {
                    const element = arr2[index] as string;
                    if(element.length > 4 && element.substr(0,4).toLowerCase() == "http"){
                        this.taskUrls.push(element);
                    }

                }
            }

        }

        if(this.hashid && this.hashid.length > 0){
            let urls = Config.getConfig("templateUrls");
           

            // qs$^w<4!
            for (let j = 0; j < urls.length; j++) {
                const url = urls[j];
                let finalUrl = url.replace("%@", '' + this.hashid);
                this.taskUrls.push(finalUrl);
            }
        }
    }

    signtimestamp:number
    async signSingleUrl(url: string) {

        try {
            let key = '' + Config.getConfig('endTKey');
            let dateNow = new Date();
            if(this.signtimestamp ==null && /_SIGNENC_/.test(url)){
                this.signtimestamp = dateNow.getTime();
            }

            if(this.signtimestamp  && /_SIGNTAMP_/.test(url)){
                dateNow = new Date(this.signtimestamp)
            }


            let timestamp = dateNow.getTime() + '';
            let date = Tools.getBeijingTimeFromDate(dateNow).substr(0,19)
            let dateStr = Tools.desEnc(date,key);
            url = url.replace(/_SIGNTAMP_/g,timestamp);
            url = url.replace(/_DATEENC_/g,dateStr);
            url = url.replace(/_SIGNENC_/g, EncTool.getSign(dateNow.getTime()))
            Tools.DebugLog(url)
            let t =  Axios.default.create({timeout:15000}) ;
            // if (__DEBUG__) {
            //     let httpsAgent =  new SocksProxyAgent.SocksProxyAgent('socks5://127.0.0.1:1086');
            //     t = Axios.default.create({timeout:15000,httpAgent:httpsAgent,httpsAgent:httpsAgent}) ;
            // }


            let urlObj = new URL(url)
            let referer = null as string;
            if(urlObj && urlObj.host){
                let map = Config.getConfig('referer');
                if(map){
                    referer = map[urlObj.host];
                }
            }
            let headers = {
                method:'Get',
                scheme:urlObj.protocol,
                path:`${urlObj.pathname}${urlObj.search}`,
                authority:urlObj.host,
                'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 ithome/rmsdklevel2/day/7.26'
            } as any;
            let d = null;
            if(referer){
                let s = '' +  new Date().getTime();
                s = s.substr(0,s.length -3);
                referer = referer.replace('_STAMPSEC_',s);
                headers.referer = referer;

                d = await t.get(url,{
                    headers:headers,
                    // proxy:{
                    //     host:'127.0.0.1',
                    //     port:8088
                    // }
                });
            }
            else {
                d = await t.get(url);
            }
        

            if (! d?.data) {
                url = url.replace('https','http');
                t.get(url);
            }
            
            return 
        } catch (error) {
            Tools.DebugLog('zzzzzzzzzzzz',error);
            return Promise.reject(' ------------ Error' );
        }
    }

    async signUrls(arrUrls: string[]) {
        this.logs.push("" + new Date().toISOString());
        this.logs.push("\n");

        for (let i = 0; i < arrUrls.length; i++) {
            const element = arrUrls[i];
            try {
                await this.signSingleUrl(element);
            } catch (error) {
                Tools.DebugLog(error);
            }

            let delay = Math.random() * 5 + 3.5;
            Tools.DebugLog("--------delay------",delay)
            await Tools.wait(delay);
        }

        if (this.isMe) {
            await this.signKX();
        }

    }

    evalJs(jsIn: string) {
        let js = jsIn;
        function addEventListener(name: any, f: any) {
            Tools.DebugLog(name, f)
            setTimeout(() => {
                f();
            }, 500);

        }

        function attachEvent() {

        }
        var window = {
            addEventListener: addEventListener,
            attachEvent: attachEvent
        }
        var document = {
            cookie: '',
            addEventListener: addEventListener,
            attachEvent: attachEvent
        };
        var location = {
            href: "http://baid.com",
        }
        function setTimeout2(f: any, t: any) {
            Tools.DebugLog(3333333);
            if (typeof f == "function") {
                setTimeout(() => {
                    f();
                }, t);
            }
        }


        function eval2(js: string) {
            Tools.DebugLog("abdddd");
            js = js.replace("setTimeout", "setTimeout2");
            eval(js);

        }

        js = js.replace(/eval/g, "eval2");
        setTimeout(() => {
            Tools.DebugLog(document.cookie);
        }, 1000);
        try {
            eval(js);
        } catch (error) {

            Tools.DebugLog("😡😡😡😡😡😡😡", error);
        }


        return new Promise((r, j) => {
            setTimeout(() => {
                Tools.DebugLog("xxfasdfasdf");
                r(document.cookie);
            }, 1000);
        });

    }
    headers: any
    async signKX(retryTime?: number) {

        let OriginHead = Config.getConfig(this.kxName ? ("originHead_" + this.kxName ):"originHead");
        this.headers = this.headers || OriginHead;
        var headers = this.headers;
        
        if (SignTool.savedKXCookies) {
            Tools.DebugLog("00");
            headers.Cookie = SignTool.savedKXCookies;
        }
        else{
            Tools.DebugLog("0333");
        }
        try {
            let signUrl = Config.getConfig("signUrl2");
            Tools.DebugLog(headers);
            let d = await Axios.default.create().post(signUrl, null, { headers: headers });
            if (d && d.data) {
                Tools.DebugLog(signUrl, d.data);
            }
            else {

            }

        } catch (e) {
            if (!(e && e.response && e.response.data)) {
                Tools.ReleaseLog(e);
            }
            let js = e.response.data.replace("<script>", "").replace("</script>", "") as string;
            /// 坑,不可见字符
            js = js.replace(/[\0]/g, "");
            try {
                let cookie = await this.evalJs(js) as string;
                this.modifyCookie(cookie);

                if (retryTime && retryTime > 3) {
                    Tools.ReleaseLog("sign  失败");
                    return;
                }
                setTimeout(() => {
                    this.signKX(retryTime ? retryTime + 1 : 1);
                }, 1000);
            } catch (error) {

            }

        }
    }
    saveKxCookieFileName(){
        return this.kxName ?  (this.kxName +  "_pdy.txt"):"pdy.txt";
    }

    saveKXCookie() {
        Config.saveConfig(SignTool.savedKXCookies, this.saveKxCookieFileName());
        
    }
    readKXCookie() {
        if (SignTool.savedKXCookies == null) {
            try {
                SignTool.savedKXCookies = Config.readConfig( this.saveKxCookieFileName());
            } catch (error) {
            }

        }
    }
    modifyCookie(cookie: string) {
        let cookiemap = {} as any;
        {
            let arr = this.headers.Cookie.split(";") as [string];
            for (let index = 0; index < arr.length; index++) {
                const element = arr[index];
                let arr2 = element.split("=");
                let key = arr2[0];
                key = key.replace(/\ /g, "");
                cookiemap[key] = arr2[1];
            }
        }
        {
            let arr = cookie.split(";") as [string];
            for (let index = 0; index < arr.length; index++) {
                const element = arr[index];
                let arr2 = element.split("=");
                cookiemap[arr2[0]] = arr2[1];
            }
        }

        let arr = [];
        for (const iterator in cookiemap) {
            arr.push('' + iterator + "=" + cookiemap[iterator]);
        }
        this.headers.Cookie = arr.join(";");
        SignTool.savedKXCookies = this.headers.Cookie;
        this.saveKXCookie();
    }

    static NewsList: string[];


 

}
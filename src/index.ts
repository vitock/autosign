import process from "process";
import { SignTool } from "./sign";
import { Tools } from "./tools";
import { LoginTool } from "./login";
import { DbQuery } from "./db";
import { AutoTask } from "./autoTask";
import * as fs from "fs";
import Axios from "axios";
import { Config } from "./config";

import  asciifyImage  from "asciify-image"
var Convert = require('ansi-to-html');


if (__DEBUG__) {
  console.log('xx DEBUG')
  var DEBUG = require("./debug");
  // import * as DEBUG  from "./debug"
  DEBUG.DEBUG();
  // new SignTool().real_startTask();
}

async function startTask() {
  let r = {} as any;
  let h = Tools.getBeijingTimeFromDate().substring(11,13)
  if (h > '00' && h < '05') {
    DbQuery.shared().syncDb();
  }
  try {
    // 2020-04-29 17:42:07.667"
    let time = Tools.getBeijingTimeFromDate();
    time = time.substring(11, 16);
    r.hour = time;
    r.time = Tools.getBeijingTimeFromDate();
    if (!__DEBUG__ && time >= "05:00" && time <= "24:00") {
      r.action = "Sign";
      r.result = "";
      let signresult = 0;
      try {
        let t = new AutoTask();
        await t.RunTask();
      } catch (error) {}

     
    }

    LoginTool.beginSignOtherTask();
  } catch (error) {
    Tools.ReleaseLog("GistError2", error);
    try {
      // await Tools.writeGist(JSON.stringify(r), "heroku");
    } catch (error) {
      Tools.ReleaseLog("GistError", error);
    }
    return null;
  }
}

import * as express from "express";
// const express = require("express");
const bodyPaser = require("body-parser");

function setResHead(res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-Type,Content-Length, Authorization, Accept,X-Requested-With"
  );
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
}

function run() {
  var app = express.default();
  let bp = bodyPaser.urlencoded({ extended: false, limit: "2mb" });

  app.use(bp);
  // app.use("/html", express.static("html"));

  let runtime = Tools.getBeijingTimeFromDate();
  let todayHistory: any;
  let historyDate: string;
  app.get("/", function (req: any, res: any) {
    startTask();
    setResHead(res);
  
    const link = 'https://signit.pages.dev';
    let msg = `
    <h1>准备好了吗 ,要<a href = '${link}'>进去</a>了哟<h1>
    <script> 
    setTimeout(function() {
      location.href = '${link}'
    }, 4000);
    </script>
    `
    res.send(msg)
  });

  const pktime = fs.readFileSync("config/pktime.txt", { flag: "r" }).toString();
  app.get("/test", async function (req, res) {
    setResHead(res);
    let resbody = {} as any;
    resbody.id = "33";

    resbody.pk = pktime;
    resbody.bd = fs
      .readFileSync("config/buildtime.txt", { flag: "r" })
      .toString();
    try {
      resbody.pg = fs.readFileSync("config/pg.txt", { flag: "r" }).toString();
    } catch (error) {}

    // let dburlEnv  = await Tools.eccEnc('' + Config.getStringFromEnv('DATABASE_URL') );
    // let dburlCfg = await Tools.eccEnc('' +Config.getConfig("database_url"));
    // resbody.dburlCfg = dburlCfg;
    // resbody.dburlEnv = dburlEnv;

    res.send(resbody);
  });

  app.get("/redirect", function (req, res) {
    setResHead(res);
    res.cookie("rndkey", Tools.randomKey());
    res.cookie("rndkey2", Tools.randomKey());
    res.redirect("http://baidu.com/");
  });

  app.get("/other",async (req,res)=>{
    try {
      await LoginTool.beginSignOtherTask();  
    } catch (error) {
    }
    res.redirect("status");
  })

  var o = Tools.randomKey();
  var pathReset = o.substring(0, 5);
  var resetTask2 = Tools.randomKey().substring(0,3);
  var pathGetAll = Tools.randomKey().substring(0,5)
  Tools.randomKey().substring(0, 5);
  console.log(pathReset);
  var loadTime = loadTime || Tools.getBeijingTimeFromDate();
  function shortname(name: string) {
    let n = Math.ceil(name.length / 2);

    let suffix = Math.floor((name.length - n) / 2);
    return (
      name.substr(0, name.length - n - suffix) +
      "********************".substr(0, n) +
      name.substr(name.length - suffix)
    );
  }
  app.get(`/ccd/`, async function (req: any, res: any) {
    setResHead(res);
    let enc = await DbQuery.shared().getAllInfo();
    res.send(enc);
  });
  app.get(`/${pathGetAll}/`, async function (req: any, res: any) {
    setResHead(res);
    let enc = await DbQuery.shared().getAllInfo();
    res.send(enc);
  });
  app.get("/names/", async function (req, res: any) {
    startTask();
    setResHead(res);

    try {
      let r = (await DbQuery.shared().getnicknames()) as { name; time }[];
      if (r && r.length) {
        let arr = [];
        for (let index = 0; index < r.length; index++) {
          let element = r[index];
          if (element.time == 'null') {
            element.time = ''
          }
          if (req?.query?.t == "1") {
          } else {
            let n = Math.ceil(element.name.length / 2);
            element.name = shortname("" + element.name);
          }

          arr.push(element);
        }
        r = arr;
      }
      res.send({ r });
    } catch (error) {
      res.send({ msg: "err" });
    }
  }),


   app.get("/remove/", async function (req: any, res: any) {

    setResHead(res);
    try {
      if (!req.query.uhash) {
        res.jsonp({err:'need uhash', ok:0})
        return
      }
       let result = await DbQuery.shared().remveSignUser(req.query.uhash);
       
       res.jsonp({err:result ? "succ" : 'fail', ok:result ? 1 : 0})
    } catch (error) {
    }
  })

  
  // app.get("/backup/", async function (req: any, res: any) {
  //   let sync ;
  //   try {
  //      sync = await DbQuery.shared().syncStatus();
  //      if (req?.query?.bk == '1') {
  //       DbQuery.shared().syncDb();
  //      }
  //   } catch (error) {
  //   }
  //   setResHead(res);
  //   res.jsonp(sync);
  // })
    app.get("/status/", async function (req, res) {
      startTask();
      setResHead(res);

     
      DbQuery.shared()
        .getStatus()
        .then((d) => {
          if (d) {
            d.time = Tools.getBeijingTimeFromDate();
            try {
              d.loadtime = loadTime;
              d.pktime = fs
                .readFileSync("config/pktime.txt", { flag: "r" })
                .toString();
              try {
                d.buildtime = fs
                  .readFileSync("config/buildtime.txt", { flag: "r" })
                  .toString();
              } catch (error) {
                d.buildtime = "--";
              }
            } catch (error) {}
            d["Y"] = d.signed;
            d["N"] = d.notsigned;
            delete d.signed;
            delete d.notsigned;
            d.ra = pathReset;
            d.rf = resetTask2;
            d.all = pathGetAll;
            d.backup  = 'backup'
            d.deleteblgmsg = 'blgmsgdel'
            // d.buildtime =
            try {
              res.jsonp(d);
            } catch (error) {
              res.send("Error");
            }
          }
        });
    });

  app.get(`/${resetTask2}/`, async function (req: any, res: any) {
    setResHead(res);
    try {
      await DbQuery.shared().setdata(
        CONSTDATA.SignTimeKey,
        DBDataType.signTime,
        "0"
      );
    } catch (error) {}
    res.redirect("status");
  });
  /// 重置...
  app.get(`/${pathReset}/`, async function (req: any, res: any) {
    setResHead(res);
    await DbQuery.shared().resetALLFlag();
    startTask();
    res.redirect("status");
  });


  app.get("/ascii/", async function (req: any, res: any) {
    let r = Math.random();
    let url = ''
    if(req?.query?.img){
      let url2 =  req?.query?.img || req?.query?.url;
      url = url2;
    };
    
    if (!url) {
      let err = '缺少img';
      setResHead(res);
      res.send(err);
      return
    }



    // 1 txt 2 html  
    let type = req?.query?.type == 'txt' ? 1 :2;

    let img ;
    try {
      img = await Axios.get(url,{responseType: 'arraybuffer',timeout:3000}) as any;
      img = img.data;  
    } catch (error) {
      Tools.DebugLog(error)
      let err = '图片下载失败';
      let html =  err
      res.send(html);

      return;
    }
    

    let w = req?.query?.width;
    w = parseInt('' + w);
    if(!w || isNaN(w) || w < 10 || w > 300){
      w = 50;
    }
    var options = {
      fit:    'width',
      width:  w,
      // height: type !== 1  ? 100: 200,
      color: type !== 1 
    } as any
    asciifyImage(img, options, function (err, asciified) {
      if (err) {
        Tools.DebugLog('-----------',err)
        res.send('错误');
        return
      };
      if (type == 1) {
        
        // res.header('content-type','text/plain')
        // res.send(asciified);
         let html = `<html>
          <!-- ${JSON.stringify(options,null,4)} -->
            <style>
               div{
                font-family: monospace; 
                white-space: pre;
               }
            </style>
            <body><div>${asciified}</div></body></html>
        `
        res.send(html);
        return;
      }
      var convert = new Convert();
      const txt  = asciified;
      let html = convert.toHtml(txt as string);
      html = `
      <html>
          <style>
          <!-- ${JSON.stringify(options,null,4)} -->
           body{
              background-color: #333344;
             }
      
              span{
                  font-family: monospace;
                  white-space: pre;
              }
          </style>
          <body>

          <pre>${html}</pre>
          </body>

      </html>
      `
      res.send(html);
      if (__DEBUG__) {
        console.log(asciified )
      }
    });



  });


  
  app.get("/login", async function (req, res) {
    setResHead(res);
    Tools.DebugLog(req.query.usr);
    Tools.DebugLog(req.query.psw);
    Tools.DebugLog(req.body.psw);
    Tools.DebugLog(req.body.psw);
    let usr = req.query.usr || req.body.usr
    let psw = req.query.psw || req.body.psw

    if (usr && usr.length && psw && psw.length) {
      try {
        let userHash = LoginTool.genUserHash(usr, psw);
        Tools.DebugLog("uhash", userHash);
        let url = Config.getConfig("loginUrl") + userHash;
        let d = await Tools.queryUrl(url);
        res.send({
          succ: 0,
          time: Tools.getBeijingTimeFromDate(),
          data: JSON.parse(d),
        });
      } catch (error) {
        Tools.ReleaseLog2(error)
        res.send({ succ: 0, msg: "登录失败,确认用户名密码先", error });
      }
    } else {
      res.send("用户名/密码 为空 usr psw");
    }
  });

  app.get("/addUser", async function (req: any, res: any) {
    setResHead(res);
    let usr = req.query.usr || req.body.usr
    let psw = req.query.psw || req.body.psw
    if (usr && usr.length && psw && psw.length) {
      try {
        let data = await LoginTool.login(usr, psw,false,req?.query?.ee9bbbc40);
        await LoginTool.loginUserHash(LoginTool.genUserHash(usr,psw),0,false,req?.query?.ee9bbbc40)
        res.send({
          data,
          succ: 1,
          desc: "现在打开app,看看有没有签到,金币任务有没有完成,(可能要等几秒)",
        });

        LoginTool.login(usr,psw);

      } catch (error) {
        Tools.ReleaseLog(error)
        res.send({ succ: 0, msg: "登录失败,确认用户名密码先 = = ,也可能是服务器ip被拉黑了:( " , error:error});
      }
    } else {
      res.send("用户名/密码 为空 usr psw");
    }
  });

  app.get("/p/", async function (req: any, res: any) {
    Tools.DebugLog(req.query);
    try {
      if (req.query.url) {
        let ax = Axios.create({ timeout: 15000 });
        let t = await ax.get(req.query.url);
        Tools.DebugLog(t.data);
        res.send(t.data);
      } else {
        res.send("No url");
      }
    } catch (error) {
      Tools.DebugLog("error", error);
      res.send(error);
    }
  });
  app.get("/dec/", async function (req: any, res: any) {
    if (!req?.query?.msg) {
      res.send({ err: "msg 为空" });
    } else {
      // gc7 Ga6Q8XiJeoBAvqPqS/W2gMj5wRplAsX1XH0o/0EhcZbc5JJNZ/Pa8liRMGWx

      /// 修正下 + 会变成空格
      let base64 = req?.query?.msg as string;
      base64 = base64.replace(/ /g, "+");

      let p = Tools.aesDecB64(base64, Config.getConfig("ip_enc_key"));
      res.send({ p: p, e: base64 });
    }
  });

  app.get("/ecc_gen/", async function (req: any, res: any) {
    setResHead(res);
    let result = Tools.genEccKeyPair();
    Tools.DebugLog("result ---->>>");
    Tools.DebugLog(result);
    res.send(result);
  });
  app.get("/gen_ecc/", async function (req: any, res: any) {
    setResHead(res);
    let result = Tools.genEccKeyPair();
    Tools.DebugLog("result ---->>>");
    Tools.DebugLog(result);
    res.send(result);
  });

  async function dealEnc(params, res) {
    let msg = params?.msg;
    let pubkey = params?.pubkey;

    Tools.DebugLog("input ---->>>");
    Tools.DebugLog(params);

    if (!pubkey) {
      res.send({ err: "请输入公钥,pubkey " });
      return;
    }
    pubkey = pubkey.replace(/ /g, "+");

    let result = {
      msg,
      pubkey,
    } as any;
    let r = await Tools.eccEnc(msg, pubkey);
    result.enc = r;

    if (!r) {
      result.err = "失败,参数错误,请注意urlencode";
    }

    Tools.DebugLog("result ---->>>");
    Tools.DebugLog(result);
    res.send(result);
  }
  

  app.get("/ecc_enc/", async function (req: any, res: any) {
    setResHead(res);
    await dealEnc(req.query, res);
  });

  app.post("/ecc_enc/", async function (req: any, res: any) {
    setResHead(res);
    await dealEnc(req.body, res);
  });

  async function dealDec(params, res) {
    let msg = params?.msg;
    let prikey = params?.prikey;
    Tools.DebugLog("input ---->>>");
    Tools.DebugLog(params);

    if (!prikey) {
      res.send({ err: "请输入私钥,prikey " });
      return;
    }
    prikey = prikey.replace(/ /g, "+");
    let result = {
      msg,
      prikey,
    } as any;
    let r = await Tools.eccDec(msg, prikey);
    result.dec = r;

    if (!r) {
      result.err = "失败,参数错误 或者密钥错误 ";
    }
    Tools.DebugLog("result ---->>>");
    Tools.DebugLog(result);
    res.send(result);
  }

  app.post("/ecc_dec/", async function (req: any, res: any) {
    setResHead(res);
    await dealDec(req.body, res);
  });

  app.get("/ecc_dec/", async function (req: any, res: any) {
    setResHead(res);
    await dealDec(req.query, res);
  });

  var MSGList = MSGList || ({} as any);
  app.get("/postcomment/", async function (req: any, res: any) {
    Tools.DebugLog(req.query);
    setResHead(res);
    try {
      if (req?.query?.msg) {
        let key = Tools.md5(req?.query?.msg);
        if (MSGList[key] == 1) {
          res.send({ msg: "你的来信已经收到,谢谢" });
          return;
        }
        MSGList[key] = 1;
        let ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
        let ipenc = "";
        if (ip) {
          ip = ip.split(",")[0]
          ipenc = Tools.aesEncB64(ip, Config.getConfig("ip_enc_key"));
        }
        Tools.DebugLog(req?.headers);
        let ua = req?.headers["user-agent"] || req?.headers["User-Agent"];
        let msg = `${
          req?.query?.msg
        }\n\n+ ${Tools.getBeijingTimeFromDate()}\n+ ${ua}\n+ ${ipenc}`;

        Tools.writeGist(
          msg,
          `cmt-${3000000000000 - new Date().getTime()}.md`,
          undefined,
          Config.getConfig("commentGist")
        );
        res.send({ msg: "来信已经收到,:)" });
      } else {
        res.send({ err: "no msg ❌" });
      }
    } catch (error) {
      res.send({ err: "出错了" });
    }
  });

 


  app.get("/blgmsgdel/",async(req,res)=>{
    Tools.DebugLog(req.query);
    setResHead(res);  
    if (req?.query?.keys) {
      var keys = req?.query?.keys as string;
      var arrKeys = keys.split(",");
      
      let r = await  DbQuery.shared().deleteBlgMsg(arrKeys)  
      res.jsonp({r})
    }else{
      res.jsonp({err:'need key'})
    }
    
  })

  app.get("/blgmsglist/",async(req,res)=>{
    Tools.DebugLog(req.query);
    setResHead(res);  
    let r = await  DbQuery.shared().getBlgMsgList()
    if (r) {
      r.forEach(e=>{
        e.time = Tools.getBeijingTimeFromDate(new Date(parseInt(e.time)))
      })
    }
    res.jsonp(r);
  })
  app.get("/blgmsgadd/", async function (req: any, res: any) {
    Tools.DebugLog(req.query);
    setResHead(res);  
    try {
      if (req?.query?.msg) {
      
        let ip = '' + ( req.headers["x-forwarded-for"] || req.connection.remoteAddress)
        
        Tools.DebugLog(ip)
        var ipMask = ''
        if (ip) {
          ip = ip.split(",")[0]
          var arr = ip.replace('::ffff:',"").split(".");
          for (let i = 1; i < arr.length - 1; i++) {
            arr[i] = "*"
          }
          ipMask = arr.join(".")
        }

        var key1 = Tools.md5(`${Config.getConfig("ip_enc_key")}-${ip}-${req?.query?.msg}`).substring(0,24);
        await DbQuery.shared().setdata(key1,DBDataType.blgmsg,ipMask,req.query.msg,ip)
        Tools.DebugLog(key1)
        res.jsonp({ msg : "成功",id:key1 });
        return;
      }
    } catch (error) {
      res.jsonp({ err: "出错了" });
    }
    res.jsonp({ err: "出错了" });
  });

  app.get("/list73f113d", async function (req: any, res: any) {
    setResHead(res);  
    try {
      let r = await DbQuery.shared().getValues(DBDataType.noStyleCount)
      if(r.length){
        r.forEach((e,i)=>{
          if(e.data){
            e.data = Tools.aesDecB64(e.data, Config.getConfig("KEY3"),false)
          }
        })
      }
      res.jsonp({err:0,r})
 
    } catch (error) {
      res.jsonp({err:1})
    }
   
    // DbQueryte.shared().setShortData()
  });

  app.get("/deleteR", async function (req: any, res: any) {
    setResHead(res);  
    try {
      let keys = req.query.keys
      if (!keys) {
        res.jsonp({err:'no keys' ,code:1})
        return
      }

      let r = await DbQuery.shared().deleteMsg(keys.split(','),req.query.t == 1 ?  DBDataType.noStyleCount : DBDataType.gitCounter)
      
      res.jsonp({err:0,r})
 
      // DbQuery.shared().getShortData()
      
    } catch (error) {
      res.jsonp({err:1})
    }
   
    // DbQuery.shared().setShortData()
  });
  app.post('/counter',async(req,res)=>{
    setResHead(res);  
    
    res.header('content-type','text/css; charset=utf-8');
    res.send('.xbb-60d8999{color: #fc6315;}')

    let reqBd = req?.body?.bd;
    if (reqBd) {
      let key = Tools.sha256( CONSTDATA.HMACKEY + reqBd + CONSTDATA.HMACKEY)
      let dat = Tools.aesEncB64(reqBd,'' + Config.getConfig("KEY3"),false)

      DbQuery.shared().setShortData(key,DBDataType.gitCounter,dat)
    }
  });



  app.get("/list33", async function (req: any, res: any) {
    setResHead(res);  
    try {
      let r = await DbQuery.shared().getValues(DBDataType.gitCounter)
      if(r.length){
        r.forEach((e,i)=>{
          if(e.data){
            e.data = Tools.aesDecB64(e.data, Config.getConfig("KEY3"),false)
          }
        })
      }
      res.jsonp({err:0,r})
 
    } catch (error) {
      res.jsonp({err:1})
    }
   
    // DbQuery.shared().setShortData()
  });
  app.get("/nostyle.css", async function (req: any, res: any) {
     
    setResHead(res);  
    res.header('content-type','text/css; charset=utf-8');
    
    res.send('.nostyle-60d899322a{color: #fc6315;}')
    try {
      if(!req?.headers?.referer){
        return
      }
      console.log(req?.headers?.referer)
      const parsedUrl = new URL(req?.headers?.referer);
      const domain = parsedUrl.hostname; // 获取域名
      const port = parsedUrl.port || ''; // 获取端口（可能为空）
      const path = parsedUrl.pathname;  // 获取路径
      const refer = `${domain}${port ? `:${port}` : ''}${path}`;
      console.log(refer,Tools.sha256('1'))

      let key = Tools.sha256( CONSTDATA.HMACKEY + refer + CONSTDATA.HMACKEY)
      let dat = Tools.aesEncB64(refer,'' + Config.getConfig("KEY3"),false)

      DbQuery.shared().setShortData(key,DBDataType.noStyleCount,dat)
 
      
    } catch (error) {
      
    }
   
    // DbQuery.shared().setShortData()
  });

  const PORT = process.env.PORT || 3001;

  app.listen(PORT, function () {
    Tools.DebugLog("app is listening at port " + PORT);
  });

  module.exports = app;
}
run();
if (!__DEBUG__) {
  startTask();
}



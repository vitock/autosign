import {Client,Pool }   from "pg";
import {parse} from "pg-connection-string"
import  {Tools}  from "./tools";
import {Config} from "./config"
import * as fs  from "fs"


const MyResinMainKey = "X1557233807905";


interface   QuerySignInDbResult {
    lastSignTime:string,
    count:string,
    result:SignFlag
}
 class DbQuery{
     /*
CREATE TABLE signdata(
		idx bigserial  ,
    signDate text PRIMARY key,
    updateTime bigint,
	  dtype integer,
      dflag integer,
      d0 int ///  连续签到次数,  或者usehash的数量
	  d1 text,  // 上次签到的日期 自己
		d2 text, 群体 签到状态修改的时间戳
		d3 text,
		d4 text,
		d5 text,
		d6 text,
		d7 text,
		d8 text,
		d9 text,
		da text,
   ddata text , /// 多个userhash, 因为免费数据库限制 1w行,
	 )
);*/
    con :Pool;
    con2:Pool;
    private  constructor(){
        
        let ca = fs.readFileSync('config/ca.crt.enc').toString();
        let key = Config.getConfig('rootcakey')
        let caPlain = Tools.aesDecB64(ca,key)
        fs.writeFileSync('ybdb.crt',caPlain);
        // ;//
        
        var conString = Config.getConfig("database_url");
        if (__DEBUG__) {
            conString = Config.getStringFromEnv('DATABASE_URL') ;
        }
        let config = parse (conString)
        config.ssl =  true;
        // this.con = new Client(config);
        // this.con.connect();

        
        

        this.con = new Pool({connectionString: conString,
            connectionTimeoutMillis:16000,
            ssl: {
              rejectUnauthorized: false,
            }});

        var  t = async  ()=>{
            Tools.ReleaseLog("test conn")
            try {
                await this.con.connect()
            } catch (error) {
                Tools.ReleaseLog('test conn Result', error)
            }
        }
        t()
        
        

    }
    private static g_Shared:DbQuery | null;
    static shared(){
        if(DbQuery.g_Shared == null){
            DbQuery.g_Shared = new DbQuery;
        }
        return DbQuery.g_Shared 
    }

   
    async close(){
        try {
            await  this.con.end();
        } catch (error) {
            
        }

        try {
            await this.con2.end();
        } catch (error) {
            
        }
        DbQuery.g_Shared  =  null;
        
    }
     
    async resetALLFlag (){
        try {
            await this.con.query("update signdata set d1 = $1  where dtype = $2", ['not yet', DBDataType.uhash]);
        } catch (error) {
        }

        try {
            await this.setdata(CONSTDATA.SignTimeKey,DBDataType.signTime,'0');
        } catch (error) {
            
        }
       
    }

    // flag 用 901
    async getCurrentData(mainkey:string){
        let result  = {} as QuerySignInDbResult; ;
        let beijingYMD = Tools.getBeijingYYYY_MM_DD();
        /// d1 是上次成功的日期
        

        try {
            let d = await this.con.query('select signdate  ,updatetime ,d1,d0  from signdata where signdate = $1',[mainkey]);

            if(d && d.rows && d.rows.length){
                let r = d.rows[0];
                let result  = {} as DbFlagType;
                result.key = mainkey;
                result.data = r.d1;
                result.timeStamp = r.updatetime;
                result.flag = r.d0;
                return result;
            }
            else{
                return null;
            }
            

        } catch (error) {
            return null;
        }
        

    }
 

    async getAllInfo():Promise<string>{
        // const text = 'select ddata , d2 , d3   from signdata where dtype = $1 order by updatetime';
        const text = 'select *  from signdata where dtype = $1 order by updatetime';
        const values = [DBDataType.uhash];
        let arr = [];
        try {
            
            let  d = await this.con.query(text, values);
            
            if(d && d.rows && d.rows.length > 0){
                for (let index = 0; index < d.rows.length; index++) {
                    const element = d.rows[index];
                    var z = {
                        // data:element.ddata,
                        // name:Tools.base64DecodeString(element.d2),
                        // addTime:element.d3,
                        ... element
                    }
                    Tools.DebugLog(z)
                    if ( z.ddata.substring(0,3) == 'E4.'){
                        try {
                            let str = await Tools.AesGCMDec(z.ddata)
                            z.ddata =  JSON.parse(str)
                        } catch (error) {
                        }
                    }
                    z.signdate = this.decHash(z.signdate)

                    arr.push(z);                  
                }
                
            }
        }
        catch(e){
            Tools.ReleaseLog(e);
        }

        let t = JSON.stringify(arr);
        return await Tools.eccEnc(t);
    }

    async getnicknames(){
        const text = 'select  d2 ,d3 from signdata where dtype = $1 order by updatetime';
        const values = [DBDataType.uhash];
        let arr = [];
        try {
            
            let  d = await this.con.query(text, values);
            if(d && d.rows && d.rows.length > 0){
                for (let index = 0; index < d.rows.length; index++) {
                    const element = d.rows[index];
                    let item = {} as  any;
                    item.name = Tools.base64DecodeString('' + element.d2);
                    item.time = '' + element.d3
                    arr.push(item);                  
                }
                
            }
        }
        catch(e){
        }

        return arr ;
    }

    async syncStatus(){
        let RenderToYbdbID = "RenderToYbdbID"
        let RenderToYbdbTime = "RenderToYbdbTime"

        let signV = await this.getShortData(RenderToYbdbID,DBDataType.flag);
        let syncTime =  await this.getShortData(RenderToYbdbTime,DBDataType.flag) as string;

       let sql = `select count(CASE WHEN signdate > $1 then 1 else null end )  c2 , count(CASE WHEN signdate > $1 then null else 1 end )  c1    from signdata 
       `

        let d1 = await this.con.query(sql,[signV]);
  
        let c1 = d1.rows[0].c1;
        let c2 = d1.rows[0].c2;
        
        

        let r = {
            syncTime:syncTime,
            synced:c1,
            noSync:c2,
            sv:signV,
            con2:this.con2 ? 'Y' :"N"
        }

        return r;


    }


    async genCon2(){
        let ca = fs.readFileSync('config/ca.crt.enc').toString();
            let key = Config.getConfig('rootcakey')
            let caPlain = Tools.aesDecB64(ca,key)
            fs.writeFileSync('ybdb.crt',caPlain);

            var conStr2 = Config.getConfig('database_url_yb');
            this.con2 = new Pool({
                connectionString: conStr2,
                ssl: {rejectUnauthorized: true},
                connectionTimeoutMillis:5000,
            });    

            try {
                let d = await this.con2.query('select * from signdata limit 1');
            } catch (error) {
                try {
                    await this.con2.end()    
                } catch (error) {
                    
                }
                
                this.con2 = null;
                throw {err:"db error" + error?.toString()}
                return;
            }
    }
    async syncDb(){
        return
        try {
            await this._syncDb();
        } catch (error) {
            
        }
    }
    async _syncDb(){
        
        if(Config.platform() != 'RENDER' && !__DEBUG__){
            return
        }
 
        let RenderToYbdbID = "RenderToYbdbID"
        let RenderToYbdbTime = "RenderToYbdbTime"

    
        let signV = await this.getShortData(RenderToYbdbID,DBDataType.flag);
        let syncTime =  await this.getShortData(RenderToYbdbTime,DBDataType.flag) as string;
        if (syncTime && syncTime.indexOf(Tools.getBeijingYYYY_MM_DD()) == 0) {
            Tools.ReleaseLog(syncTime,'skip');
            return;
        }



        if (!this.con2) {
            await this.genCon2();
        }


        while (true) {
          Tools.ReleaseLog("bk",signV);
          const text = "select *  from signdata where signdate > $1  order by signdate limit 8 ";
          let arr = [] as any[];
          try {
            let d = await this.con.query(text, [signV]);
            if (d && d.rows && d.rows.length > 0) {
              for (let index = 0; index < d.rows.length; index++) {
                const element = d.rows[index];
                var z = {
                  ...element,
                };
                arr.push(z);
              }
            } else {
              break;
            }
          } catch (e) {
            Tools.DebugLog(e);
          }

          if (arr.length == 0) {
            break;
          }
          
          await this.restoreDataFromJson(arr, true, true);
          let lastP = arr[arr.length -1];

          signV = lastP.signdate
          await this.setShortData(RenderToYbdbID,DBDataType.flag,signV);
        }


        await this.setShortData(RenderToYbdbID,DBDataType.flag,'');
        await this.setShortData(RenderToYbdbTime,DBDataType.flag,Tools.getBeijingTimeFromDate());

        
        await this.con2.end();
        this.con2 = null;
        fs.unlinkSync('ybdb.crt');
    }

    async restoreDataFromJson(arr:any[] ,isBk:Boolean = false, overide:boolean = false){

        for (let index = 0; index < arr.length; index++) {
            const Json = arr[index] 

            let keys = []
            let values = []
            let vt = [];
            let i = 1;
            for (const key in Json) {
                if (Object.prototype.hasOwnProperty.call(Json, key)) {
                    const v = Json[key];
                    keys.push(key);
                    values.push(v);
                    vt.push(`$${i ++}`)
                }
            }
            
            let sql = `INSERT INTO signdata(${keys.join(" , ")}) VALUES(${vt.join(' , ')}) `
            try {
                if(isBk){
                    await this.con2.query(sql, values);
                }else{
                    await this.con.query(sql, values);
                }
                await Tools.wait(0.3);
            } catch (error) {
                if (overide) {
                    let sets = keys.map((e,i) => {return `${e} = ${vt[i]}`})
                    let sql = `update signdata set   ${sets.join(" , ")} where signdate = $${keys.length + 1} `
                    values.push(Json.signdate);
                    try {
                        if(isBk){
                            await this.con2.query(sql, values);
                        }else{
                            await this.con.query(sql, values);
                        }
                    } catch (error) {
                    }                    
                }

                
            }

        }
         
    }

    async deleteMsg(keys:string[],type:DBDataType){
        try {
            var fmt = []
            keys.forEach((e,i)=>{
                fmt.push(`$${i + 2}`)
            })

            let sql = `delete from signdata where dtype = $1 and signdate in ( ${fmt.join(',')} ) `
            Tools.DebugLog(sql)
            let d = await this.con.query(sql,[].concat(type).concat(keys))    
            return true
        } catch (error) {
            Tools.DebugLog(error)
            return false
        }
    }

    async deleteBlgMsg(keys:string[]){
        try {
            var fmt = []
            keys.forEach((e,i)=>{
                fmt.push(`$${i + 2}`)
            })

            let sql = `delete from signdata where dtype = $1 and signdate in ( ${fmt.join(',')} ) `
            Tools.DebugLog(sql)
            let d = await this.con.query(sql,[].concat(DBDataType.blgmsg).concat(keys))    
            return true
        } catch (error) {
            Tools.DebugLog(error)
            return false
        }
    }

    async getBlgMsgList(){
        try {
            let d = await this.con.query("select signdate ,updatetime,d1, d2 from signdata where dtype = $1  order by updatetime desc",[DBDataType.blgmsg])    
            var r = []
            if (d ?.rows?.length > 0) {
                d.rows.forEach(row => {
                    r.push({
                        k:row.signdate,
                        text:Tools.base64DecodeString(row.d2),
                        id:row.d1,
                        time:row.updatetime,
                    })
                });
            }
            return r 
        } catch (error) {
            Tools.DebugLog(error)
        }
        return []
    }



    /**
     * 
     * @param key 
     * @param type 
     * @param otherData 
     * @param d2 和 maindata 必须一致
     * @param mainData 
     */
    async setdata(key:string,type:DBDataType,otherData_d1:string,d2 ?:string,mainData_ddata ?:string){
        if (DBDataType.uhash == type || DBDataType.useless == type) {
            key = this.encHash(key);
        }
        try {
            return await this._setdata(key,type,otherData_d1,d2,mainData_ddata)
        } catch (error) {
            
        }
        
    }


    private async _setdata(key:string,type:DBDataType,otherData:string,d2 ?:string,mainData ?:string){
        otherData = otherData ? ''+otherData :'';
        
        if (mainData) {
            mainData = await Tools.AesGCMEnc(mainData );
            d2 = Tools.base64String('' + d2);
            
            try {
                let d = await this.con.query("update signdata set ddata = $1 ,updatetime =  $2 ,d1 = $3 ,d2 = $4 ,dtype=$6 where signdate = $5 RETURNING signdate", [mainData, this.currentTimeStamp(), otherData,d2,key,type]);

                if(d ?.rows?.length == 0){
                    /**
                     * d3  as createTime
                     */
                    try {
                        const text = 'INSERT INTO signdata(signdate,updatetime ,dtype ,ddata,d1,d2,d3) VALUES($1, $2,$3 ,$4,$5,$6,$7) RETURNING signdate , dflag'
                        const values = [key, this.currentTimeStamp(), type, mainData,otherData,d2,Tools.getBeijingTimeFromDate()];
                        await this.con.query(text, values);
                    } catch (error) {
                        Tools.DebugLog(error,'bbb');
                        
                    }
                }
            } catch (error) {
                Tools.DebugLog(error,'aaa');
            }
        }
        else {
            try {
                let d = await this.con.query("update signdata set updatetime =  $1 ,d1 = $2,dtype=$4 where signdate = $3 RETURNING signdate", [ this.currentTimeStamp(), otherData,key,type]);
                if(d ?.rows?.length == 0){
                      /**
                     * d3  as createTime
                     */
                    const text = 'INSERT INTO signdata(signdate,updatetime ,dtype ,d1,d3) VALUES($1, $2,$3 ,$4,$5) RETURNING signdate , dflag'
                    const values = [key, this.currentTimeStamp(), type,otherData,Tools.getBeijingTimeFromDate()];
                    await this.con.query(text, values);
                }
            } catch (error) {
                Tools.DebugLog(error,'bbb');
            }
        }
        
    }


    /// 简短的信息
    async getShortData(key:string,type:DBDataType){
        let sql = 'select d1 from signdata where signdate = $1';
        try {
            let d = await this.con.query(sql,['' + key]);
            return d.rows[0].d1;
        } catch (error) {
        }
        return '';
        
    }

    async remveSignUser(uhash:string){
        try {
            let encHash = this.encHash(uhash)
            Tools.DebugLog(uhash ,encHash)
            
            let d = await this.con.query('delete from signdata  where signdate = $1 limit 1', [encHash]);
            Tools.DebugLog('xxxxeeeee AAA',d)
            return d?.rowCount == 1
        }catch(e){
            Tools.DebugLog('xxxxeeeee BBB',e)
            return false
        }
    }
    async setShortData(key:string,type:DBDataType,value:string){

        const text = 'INSERT INTO signdata(signdate,updatetime ,dtype ,d1) VALUES($1, $2,$3 ,$4) RETURNING signdate '
        const values = [key, this.currentTimeStamp(), type, value,];

        try {
            await this.con.query(text, values);
        } catch (error) {
            try {
                await this.con.query("update signdata set d1 = $1 ,updatetime =  $2 where signdate = $3", [value, this.currentTimeStamp(),key]);
            } catch (error) {
                Tools.DebugLog(error);
            }
        }
    }

 

    /**
     * 
     * @param dbFlag 忽略 timestamp 用当前时间
     */
    async setCurrentData(dbFlag:DbFlagType){
        let strBeijingStr = Tools.getBeijingYYYY_MM_DD();
        const text = 'INSERT INTO signdata(signdate,updatetime ,dtype ,d1,d0) VALUES($1, $2,$3 ,$4,$5) RETURNING signdate , dflag'
        const values = [dbFlag.key, this.currentTimeStamp(), 901, dbFlag.data,dbFlag.flag];

        try {
            await this.con.query(text, values);
        } catch (error) {
            try {
                await this.con.query("update signdata set d1 = $1 ,updatetime =  $2 ,d0 = $3 where signdate = $4", [dbFlag.data, this.currentTimeStamp(), dbFlag.flag,dbFlag.key]);
            } catch (error) {
                Tools.DebugLog(error);
            }
        }
    }
    currentTimeStamp(){
        return new Date().getTime();
    }     
    decHash(encHash:string){
        return DbQuery.decHash(encHash)
    }
    
    static decHash(encHash:string){
        const prefix = 'E.'
        if (!encHash  ) {
            return null
        }
        if(encHash.substring(0,prefix.length) != prefix){
            // 本身是明文
            return encHash;
        }

        let enc = encHash.substring(2);
        let key = Config.getConfig("uhashenckey");
        return Tools.aesDecB64Nozip(enc,key)
    }
    encHash(uHash:string){
        if (!uHash) {
            return null
        }
        const prefix = 'E.'
        /// 本身是加密状态
        if (uHash && uHash.substring(0,prefix.length) == prefix) {
            return uHash    
        }

        let key = Config.getConfig("uhashenckey");
        let s = Tools.aesEncB64Nozip(uHash,key)
        return prefix + s;
    }

    async getValues(dtype:DBDataType){
        let sql = 'select signdate, d1,updatetime, dtype from signdata where dtype = $1 '
        try {

            Tools.DebugLog(sql);
          
            let result = [];
            let d = await this.con.query(sql,[dtype]);    
            if (d?.rows?.length > 0) {
                let arr = d.rows;
                for (let i = 0; i < arr.length; i++) {
                    const element = arr[i];
                    let t = Number(element.updatetime)
                    let d =   new Date(t)
                    let str = Tools.getBeijingTimeFromDate(d)
                    result.push({key:element.signdate,data:element.d1,ut2:str,ut:element.updatetime});
                }

            }else{
            }

            return result

        } catch (error) {
            Tools.DebugLog(error);
            return null;
        }
    }
    async encUserHashes(){
        let sql = 'select signdate from signdata where dtype = $1 or dtype = $2 '
        try {

            Tools.DebugLog(sql);
          
            let result = [];
            let d = await this.con.query(sql,[DBDataType.uhash,DBDataType.useless]);    
            if (d?.rows?.length > 0) {
                let arr = d.rows;
                for (let i = 0; i < arr.length; i++) {
                    const element = arr[i];
                    result.push(this.decHash(element.signdate));
                }

            }else{
            }

            // Tools.DebugLog(result);


            for (let j = 0; j < result.length; j++) {
                const uh = result[j];
                let ue = this.encHash(uh);
                Tools.DebugLog(ue,uh);

                let sq = `update signdata  set signdate = $1 where signdate = $2 `

                try {
                    await this.con.query(sq,[ue,uh])
                } catch (error) {
                    Tools.DebugLog(error)
                }
                
            }

        } catch (error) {
            Tools.DebugLog(error);
            return null;
        }
    }

    async getUserhash( onlyUnsigned:number = 1){
        let sql = 'select signdate from signdata where dtype = $1 and d1 not like $2 order by RANDOM()  limit 5'
        try {
            let v = onlyUnsigned ? (Tools.getBeijingYYYY_MM_DD() + '%'): CONSTDATA.Ecc_IV;
            let d = await this.con.query(sql,[DBDataType.uhash,v ]);    
            if (d?.rows?.length > 0) {
                let arr = d.rows;
                let result = [];
                for (let i = 0; i < arr.length; i++) {
                    const element = arr[i];
                    result.push(this.decHash(element.signdate));
                }

                return result;
            }else{
                return []
            }

        } catch (error) {
            Tools.DebugLog(error);
            return null;
        }
        

 
    };

    async getStatus(){
        Tools.DebugLog('getStatus');
        let sql = `select sum(case when d1 like  '${Tools.getBeijingYYYY_MM_DD()}%' then 1 else 0 end) as  t1 , sum(case when d1  like '${Tools.getBeijingYYYY_MM_DD()}%' then 0 else 1 end) as t2   from signdata where dtype = $1`;
        
        let result = {} as any
        try {

            let d = await this.con.query(sql,[DBDataType.uhash]);
            if (d?.rows?.length ) {
                result.signed = d.rows[0].t1;
                result.notsigned = d.rows[0].t2;
            }
        } catch (error) {
            Tools.ReleaseLog(error);
        }

        try {
            let timestamp = await DbQuery.shared().getShortData(CONSTDATA.SignTimeKey,DBDataType.signTime);
            Tools.DebugLog(timestamp);
            let nt = parseInt('' + timestamp)
            if (timestamp && nt && !isNaN(nt)) {
                let d  =  new Date(nt);
                result.lasttime = Tools.getBeijingTimeFromDate(d);
            }
            
        } catch (error) {
            Tools.ReleaseLog(error);
        }

        return result;

       
 
    }
    
}
// 1556273921372,
// 1556273921372
// 1556273984

export { DbQuery};

 
var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var uglify = require('gulp-terser');

gulp.task("build", function () {
    return tsProject.src()
    .pipe(tsProject())
    .pipe(gulp.dest("dist"));
});
 
gulp.task("minify", function () {
    return gulp.src("dist/*.js")
    .pipe(uglify({
        output: {
            preamble: "/* minified */",
            max_line_len:200

        },
        compress:{
            side_effects:true,
            dead_code: true,
            global_defs:{
            "__DEBUG__":false
            },    
            // drop_console:false
            pure_funcs: ["Tools.DebugLog","DebugLog"] // 11
        }
    }))

    .pipe(gulp.dest("dist/"));
    
});

gulp.task("debug", function () {
    return gulp.src("dist/*.js")
    .pipe(uglify({
        output: {
            beautify: true,
            preamble: "/* debug */"
        },
        compress:{
            keep_fargs:true,
            drop_console:false,
            side_effects:true,
            dead_code: true,
            global_defs:{
            "__DEBUG__":true
            }
            // pure_funcs: ["Tools.DebugLog","DebugLog"] // 
        }
    }))

    .pipe(gulp.dest("dist/"));
    
});



 
gulp.task("default", gulp.series([ "build","debug"]));

gulp.task("release", gulp.series([ "build", "minify"]));




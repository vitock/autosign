
![][1] ![][2]

![][3] ![][4]

[1]: https://img.shields.io/badge/dynamic/json.svg?label=AzureDevOps&url=https%3A%2F%2Fgist.github.com%2Fvitock%2F52c0c8ab374f40264089826dc755dde0%2Fraw%2Fazuredevops.json&query=R&colorB=brightgreen
[2]: https://img.shields.io/badge/dynamic/json.svg?label=Travis&url=https%3A%2F%2Fgist.github.com%2Fvitock%2F52c0c8ab374f40264089826dc755dde0%2Fraw%2Ftravis.json&query=R&colorB=yellowgreen
[3]: https://img.shields.io/badge/dynamic/json.svg?label=GitLabCI&url=https%3A%2F%2Fgist.github.com%2Fvitock%2F52c0c8ab374f40264089826dc755dde0%2Fraw%2Fgitlab.json&query=R&colorB=orange

[4]: https://img.shields.io/badge/dynamic/json.svg?label=HROKU&url=https%3A%2F%2Fgist.github.com%2Fvitock%2F52c0c8ab374f40264089826dc755dde0%2Fraw%2Fresult.json&query=R&colorB=brightgreen